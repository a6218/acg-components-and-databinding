import { Component } from '@angular/core';
import { ServerElement } from './models/server-element.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  serverElements: Array<ServerElement> = [];

  onServerCreated(serverElement: ServerElement) {
    this.serverElements.push(serverElement);
  }

  onBlueprintCreated(blueprintElement: ServerElement) {
    this.serverElements.push(blueprintElement);
  }
}
