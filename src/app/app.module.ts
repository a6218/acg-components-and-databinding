import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CockpitComponent } from './components/cockpit/cockpit.component';
import { ServerElementComponent } from './components/server-element/server-element.component';
import { GameComponent } from './components/game/game.component';
import { GameControllerComponent } from './components/game/game-controller/game-controller.component';
import { GameOddComponent } from './components/game/game-odd/game-odd.component';
import { GameEvenComponent } from './components/game/game-even/game-even.component';

@NgModule({
  declarations: [
    AppComponent,
    CockpitComponent,
    ServerElementComponent,
    GameComponent,
    GameControllerComponent,
    GameOddComponent,
    GameEvenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
