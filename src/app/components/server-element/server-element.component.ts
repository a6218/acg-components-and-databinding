import { Component, OnInit, Input } from '@angular/core';
import { ServerElement } from 'src/app/models/server-element.model';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.scss']
})
export class ServerElementComponent implements OnInit {
  @Input() serverElements: Array<ServerElement> | undefined;

  constructor() { }

  ngOnInit(): void {
  }
}
