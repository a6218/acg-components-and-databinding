import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ServerElement } from 'src/app/models/server-element.model';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<ServerElement>();
  @Output() blueprintCreated = new EventEmitter<ServerElement>();

  newServerName: string = '';
  @ViewChild('newServerContent') newServerContent: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  onAddServer() {
    if (!this.newServerName || !this.newServerContent) {
      return;
    }

    const serverElement: ServerElement = new ServerElement(
      'server',
      this.newServerName,
      this.newServerContent.nativeElement.value,
    );
    this.serverCreated.emit(serverElement);
  }

  onAddBlueprint() {
    if (!this.newServerName || !this.newServerContent) {
      return;
    }

    const blueprintElement: ServerElement = new ServerElement(
      'blueprint',
      this.newServerName,
      this.newServerContent.nativeElement.value,
    );
    this.blueprintCreated.emit(blueprintElement);
  }
}
