import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  gameStatus: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  onGameStatus(status: boolean) {
    console.log('GameStatus: ', status);
    this.gameStatus = status;
  }
}
