import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-controller',
  templateUrl: './game-controller.component.html',
  styleUrls: ['./game-controller.component.scss']
})
export class GameControllerComponent implements OnInit {
  @Output() gameStatus = new EventEmitter<boolean>();

  interval;

  constructor() { }

  ngOnInit(): void {
  }

  onStart(): void {
    this.interval = setInterval(() => {
        this.startInterval();
      }, 1000, this.gameStatus
    );
  }

  isEven(n: number): boolean {
    return n % 2 == 0;
  }

  startInterval(): void {
    const randomNumber: number = Math.floor(Math.random() * 100);
    console.log('randomNumber: ', randomNumber);
    const even: boolean = randomNumber % 2 == 0 ? true : false;
    this.gameStatus.emit(even);
  }

  onStop(): void {
    clearInterval(this.interval);
    this.gameStatus.emit(false);
  }
}
