import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-even',
  templateUrl: './game-even.component.html',
  styleUrls: ['./game-even.component.scss']
})
export class GameEvenComponent implements OnInit {
  @Input() isEvenNumber: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
