import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-odd',
  templateUrl: './game-odd.component.html',
  styleUrls: ['./game-odd.component.scss']
})
export class GameOddComponent implements OnInit {
  @Input() isOddNumber: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }
}
